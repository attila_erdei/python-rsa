# Python RSA Chat#

## Usage
**Client**
```
#!bash
clientrsa.py [IP] [PORT] [NICKNAME]
```
**Server**
```
#!bash
serverrsa.py [PORT] [NICKNAME]
```
## How does it work?

The server and client take turns to exchange messages. Every message gets encrypted with the sender's public keypair (n,e). Upon arrival the messages are decrypted using the corresponding private keys(n,d).

## Structure ###

Upon instantiation, the RSA key generation algorithm is run.

- Prime numbers **p** and **q** are generated usin the Miller-Rabin primality test
- Modulus **n** (keylength) is computed by n=p*q
- Euler's totient function is computed using **phi**=(p-1)*(q-1)
- An **e** is chosen where e and phi are coprime
- Private key exponent **d** is computed by solving d*e = 1 (mod phi) using the extended Euclidean algorithm.

## Methods

**miller_rabin_is_prime**

The Miller-Rabin primality test determines whether a number is a prime.

A number n>2 is NOT a prime number, if we can find an a such that a^d = 1 (mod n) or a^(2^r) = -1 (mod n) for all  all 0 ≤ r ≤ s − 1.

The implementation is built on the pseudocode below: 

```
#!bash

Input: n > 3, an odd integer to be tested for primality;
Input: k, a parameter that determines the accuracy of the test
Output: composite if n is composite, otherwise probably prime
write n − 1 as 2r·d with d odd by factoring powers of 2 from n − 1
WitnessLoop: repeat k times:
   pick a random integer a in the range [2, n − 2]
   x ← ad mod n
   if x = 1 or x = n − 1 then do next WitnessLoop
   repeat r − 1 times:
      x ← x2 mod n
      if x = 1 then return composite
      if x = n − 1 then do next WitnessLoop
   return composite
return probably prime
```
**euclidean_gcd**

A method for finding the greatest common divisor using the Euclidean algorithm.

The implementation is built on the following pseudocode:

```
#!bash

function gcd(a, b)
    while b ≠ 0
       t := b; 
       b := a mod b; 
       a := t; 
    return a; 
```
**extended_euclidean**

An extension of the Euclidean algorithm used for computing multiplicative inverses, in this case acquiring the private key exponent d.

The implementation is a modification of the following algorithm:


```
#!bash

function inverse(a, n)
    t := 0;     newt := 1;    
    r := n;     newr := a;    
    while newr ≠ 0
        quotient := r div newr
        (t, newt) := (newt, t - quotient * newt) 
        (r, newr) := (newr, r - quotient * newr)
    if r > 1 then return "a is not invertible"
    if t < 0 then t := t + n
    return t
```

**mod_pow_rtl**

Modular exponentiation is a computationally intensive process. Implementing the right-to-left binary method greatly improves the performance.

The implementation is built on the following algorithm:

```
#!bash

function modular_pow(base, exponent, modulus)
    if modulus = 1 then return 0
    Assert :: (modulus - 1) * (modulus - 1) does not overflow base
    result := 1
    base := base mod modulus
    while exponent > 0
        if (exponent mod 2 == 1):
           result := (result * base) mod modulus
        exponent := exponent >> 1
        base := (base * base) mod modulus
    return result
```
**chinese_remainder**

The decryption function untilizes the Chinese Remainder Theorem which greately speeds up the decryption of messages.

##Encryption and decryption:

**Encryption**

![encrypt.png](https://bitbucket.org/repo/ndyX8z/images/1896458933-encrypt.png)

Using the mod_pow_rtl method.

**Decryption**

![decrypt.png](https://bitbucket.org/repo/ndyX8z/images/1610699134-decrypt.png)

Using the chinese_remainder method.

##Chat client and server

The chat server and client use **socket** low-level networking interface. After exchanging nicknames and keypairs, they begin their communication. They have to take turns as stdin locks the stdout of the terminal. This could not be overcome by using the **select** module. Altough a GUI implementation would solve the issue, it goes beyond the scope of this project.

##Links

- RSA cryptosystem: https://en.wikipedia.org/wiki/RSA_(cryptosystem) 
- Miller Rabin primality test: http://mathworld.wolfram.com/Rabin-MillerStrongPseudoprimeTest.html
- Euclidean algorithm: http://mathworld.wolfram.com/EuclideanAlgorithm.html
- RTL modular exponentiation: https://en.wikipedia.org/wiki/Modular_exponentiation#Right-to-left_binary_method
- Chinese Remainder Theorem: http://mathworld.wolfram.com/ChineseRemainderTheorem.html