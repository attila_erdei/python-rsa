# Echo client program
import socket
import sys
import rsa_system
import pickle

HOST = sys.argv[1]
PORT = sys.argv[2]  
nick= sys.argv[3]     
   
s = None
for res in socket.getaddrinfo(HOST, PORT, socket.AF_UNSPEC, socket.SOCK_STREAM):
    af, socktype, proto, canonname, sa = res
    #print(af)
    try:
        s = socket.socket(af, socktype, proto)
    except OSError as msg:
        s = None
        continue
    try:
        s.connect(sa)                          #Connect
        print("Connected to:('",HOST,"', ",PORT,')',sep='')
    except OSError as msg:
        s.close()
        s = None
        continue
    break
if s is None:
    print('Error: Could not open socket.')
    sys.exit(1)
    
s.sendall(bytes(nick,"utf-8"))                              #Send nickname.
snick=s.recv(3000)                                          #Receive nickname.
print("Connected to",snick.decode("utf-8"))
publickeys=s.recv(65507)                                    #Receive keypair.
publickeys=pickle.loads(publickeys)                         #Deserialize 
print("Received public keypair from ",snick.decode("utf-8"))

print("Generating RSA keys...")
keys=rsa_system.RSA(1024,5)                            		#Get RSA keys
ownpublickeys=[keys.e,keys.n]                              #Get public RSA keys
#print("Public keypair after pickling: ",sys.getsizeof(publickeys))

s.sendall(pickle.dumps(ownpublickeys))                       #Send keypair.

while True:
    inp=input("Message:")
    s.sendall(pickle.dumps(keys.encrypt_rsa(inp,publickeys[0],publickeys[1]))) #Encrypt and serialize
    print(nick,": ",inp)
    recvdata=s.recv(65507)
    data = keys.decrypt_rsa(pickle.loads(recvdata))		#Deserialize and decrypt
    print(snick.decode("utf-8"),": ",data)
s.close()
# print('Received', repr(data))
