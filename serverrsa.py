import socket
import sys
import rsa_system
import pickle

HOST = '0.0.0.0'           # All available interfaces
PORT = sys.argv[1]  
nick= sys.argv[2]      
s = None

for res in socket.getaddrinfo(HOST, PORT, socket.AF_UNSPEC,
                              socket.SOCK_STREAM, 0, socket.AI_PASSIVE):
    af, socktype, proto, canonname, sa = res #family, type, proto, canonname, sockaddr
    #print(sa)
    try:
        s = socket.socket(af, socktype, proto)
    except socket.error as msg:
        s = None
        continue
    try:
        s.bind(sa)
        s.listen(1)
    except socket.error as msg:
        s.close()
        s = None
        continue
    break
    
if s is None:
    print ("Error: Could not open socket.")
    sys.exit(1)
    
conn, addr = s.accept()
print ("Connected by:",addr)                    #Connect

clnick = conn.recv(3000)                        #Receive nickname
print (clnick.decode("utf-8"),"connected.")
conn.send(bytes(nick,"utf-8"))                  #Send nickname

print("Generating RSA keys...")
keys=rsa_system.RSA(1024,5)             		#Get RSA keys
ownpublickeys=[keys.e,keys.n]                   #Get own public RSA keys
#print("Public keypair after pickling: ",sys.getsizeof(publickeys))

conn.send(pickle.dumps(ownpublickeys))          #Send keypair

publickeys = conn.recv(65507)                   #Receive keypair
publickeys=pickle.loads(publickeys)   			#Deseriazlize

print ("Received public keypair from",clnick.decode("utf-8"))
#print(publickeys)
while True:
    recvdata=conn.recv(65507)
    data = keys.decrypt_rsa(pickle.loads(recvdata)) #Deserialize and decrypt
    print (clnick.decode("utf-8"),": ",data)
    sys.stdout.flush()
    if not data: break
    inp=input("Message:")
    conn.send(pickle.dumps(keys.encrypt_rsa(inp,publickeys[0],publickeys[1]))) #Encrypt and serialize
    print(nick,": ",inp)
    sys.stdout.flush()
conn.close()